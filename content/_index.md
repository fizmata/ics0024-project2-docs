# ICS0024 Project2 Docs
![As if Intelligent Wojak](https://gitlab.com/fizmata/ics0024-project2-docs/-/raw/master/static/brainet.png)

---

**Table of contents**
* Frameworks/Tools used
* Description of tests

### Frameworks/Tools used

We have 3 different repositories using different techniques for testing:
* [Selenium in Java](https://gitlab.cs.ttu.ee/arkris/ics0024-selenium)
* [Cypress](https://gitlab.cs.ttu.ee/arkris/ics0024-cypress)
* [Selenium in Python](https://gitlab.cs.ttu.ee/arkris/ics0024-selenium-python)

### Description of tests

#### Hover - Are we at home
This test checks if current page is https://the-internet.herokuapp.com

#### Hover - Are we at hovers
This test checks if current page is https://the-internet.herokuapp.com/hovers

#### Hover - No hover
This test checks if View profile option is present\
![picture](https://gitlab.com/fizmata/ics0024-project2-docs/-/raw/master/static/no_hover.png)

#### Hover - Hover click
This test checks if the following profile url exsists (not profile itself)\
![picture](https://gitlab.com/fizmata/ics0024-project2-docs/-/raw/master/static/hover_click.png)

#### Dynamic Controls - Are we at home
This test checks if current page is https://the-internet.herokuapp.com

#### Dynamic Controls - Are we at dynamic controls
This test checks if current page is https://the-internet.herokuapp.com/dynamic_controls

#### Dynamic Controls - Checkbox
* verifies that box is unchecked
* checks the box
* verifies that box is checked
![picture](https://gitlab.com/fizmata/ics0024-project2-docs/-/raw/master/static/checkbox.png)

#### Dynamic Controls - Remove
* removes checkbox
* verifies checkbox does not exsist
![picture](https://gitlab.com/fizmata/ics0024-project2-docs/-/raw/master/static/remove.png)

#### Dynamic Controls - Add
* removes checkbox
* verifies checkbox does not exsist
* adds checkbox
* verifies checkbox exsists
![picture](https://gitlab.com/fizmata/ics0024-project2-docs/-/raw/master/static/add.png)

#### Dynamic Controls - Disable
* verifies input field is disabled
![picture](https://gitlab.com/fizmata/ics0024-project2-docs/-/raw/master/static/disable.png)
* enables input field
* verifies input field is enabled
![picture](https://gitlab.com/fizmata/ics0024-project2-docs/-/raw/master/static/disable2.png)

#### Dynamic Controls - Input 
* verifies input field is disabled
* enables input field
* verifies input field is enabled
* enter text
* verifies text is there
![picture](https://gitlab.com/fizmata/ics0024-project2-docs/-/raw/master/static/input.png)







